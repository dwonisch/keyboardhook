﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace KeyboardHook {
    internal static class NativeHook {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hmod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        public delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        private static IntPtr hookId = IntPtr.Zero;
        private static readonly IntPtr keyDownEventId = new IntPtr(0x0100);
        private static EventHandler<KeyEventArgs> keyDown;

        private static void Hook() {
            if (hookId != IntPtr.Zero)
                return;

            using (Process process = Process.GetCurrentProcess()) {
                using (ProcessModule module = process.MainModule) {
                    hookId = SetWindowsHookEx(0xD, HookCallback, GetModuleHandle(module.ModuleName), 0);
                }
            }
        }

        private static void Unhook() {
            UnhookWindowsHookEx(hookId);
        }

        public static event EventHandler<KeyEventArgs> KeyDown {
            add {
                if (keyDown == null) {
                    Hook();
                }

                keyDown += value;
            }
            remove {
                keyDown -= value;

                if (keyDown == null) {
                    Unhook();
                }
            }
        }

        private static IntPtr HookCallback(int code, IntPtr eventId, IntPtr keyCode) {
            if (code >= 0 && eventId == keyDownEventId) {
                var keyDownEvent = keyDown;
                if (keyDownEvent != null)
                    keyDownEvent(null, new KeyEventArgs(Marshal.ReadInt32(keyCode)));
            }
            return CallNextHookEx(hookId, code, eventId, keyCode);
        }
    }
}