﻿using System;

namespace KeyboardHook {
    public class KeyEventArgs : EventArgs {
        public int Key { get; }

        public KeyEventArgs(int key) {
            Key = key;
        }
    }
}