﻿using System;
using System.Collections.Generic;

namespace KeyboardHook {
    public class GlobalHook : IDisposable {
        private readonly List<HotKeyRegistration> callbacks = new List<HotKeyRegistration>();
        public GlobalHook() {
        }

        public HotKeyRegistration RegisterHotkey(int key, Action callback) {
            if (callbacks.Count == 0) {
                NativeHook.KeyDown += NativeHook_KeyDown;
            }

            var hotKeyRegistration = new HotKeyRegistration(key, callback);
            callbacks.Add(hotKeyRegistration);
            return hotKeyRegistration;
        }

        private void NativeHook_KeyDown(object sender, KeyEventArgs e) {
            foreach (var c in callbacks) {
                if (c.Key == e.Key) {
                    c.Action();
                }
            }
        }

        public void UnRegisterHotkey(HotKeyRegistration registration) {
            if (callbacks.Remove(registration)) {
                if (callbacks.Count == 0) {
                    NativeHook.KeyDown -= NativeHook_KeyDown;
                }
            }
        }

        public void Dispose() {
            NativeHook.KeyDown -= NativeHook_KeyDown;
        }
    }
}
