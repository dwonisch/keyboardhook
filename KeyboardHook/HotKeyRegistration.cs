﻿using System;

namespace KeyboardHook {
    public class HotKeyRegistration {
        public int Key { get; }
        public Action Action { get; }

        public HotKeyRegistration(int key, Action action) {
            Key = key;
            Action = action;
        }
    }
}